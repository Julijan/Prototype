﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (GravityBody))]
public class CameraPointMovementController : MonoBehaviour {
	
	public float walkSpeed = 6;
    public Rigidbody rigidbody;

    private Vector3 moveAmount;
	private Vector3 smoothMoveVelocity;
	
	void Update() {
		float inputX = Input.GetAxisRaw("Horizontal");
		float inputY = Input.GetAxisRaw("Vertical");
		Vector3 moveDir = new Vector3(inputX,0, inputY).normalized;
		Vector3 targetMoveAmount = moveDir * walkSpeed;
		moveAmount = Vector3.SmoothDamp(moveAmount,targetMoveAmount,ref smoothMoveVelocity,.35f);
	}
	
	void FixedUpdate() {
		Vector3 localMove = transform.TransformDirection(moveAmount) * Time.fixedDeltaTime;
		rigidbody.MovePosition(rigidbody.position + localMove);
	}
}
