﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {
    
    public float RotationSpeed;

    void Update () {
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(Vector3.down, RotationSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(Vector3.up, RotationSpeed * Time.deltaTime);
        }
        
    }
}
