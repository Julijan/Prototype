﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poplers
{
    public class PathFinding : MonoBehaviour
    {
        GridManager grid;
        private float pathFindingRadius = 10f;
        public TerraformingManager TerraformingManager;
        public PathRequestManager PathRequestManager;

        void Awake() {
            grid = GetComponent<GridManager>();
        }

        public void StartFindPath(Vector3 startPos, Vector3 targetPos) {
            StartCoroutine(FindPath(startPos, targetPos));
        }

        IEnumerator FindPath(Vector3 startPos, Vector3 targetPos) {

            Vector3[] waypoints = new Vector3[0];
            bool pathSuccess = false;

            Node startNode = grid.NodeFromWorldPoint(startPos);
            Node targetNode = grid.NodeFromWorldPoint(targetPos);

            if (startNode.walkable && targetNode.walkable) {

                Heap<Node> openSet = new Heap<Node>(grid.MeshFilter.mesh.vertexCount);
                HashSet<Node> closedSet = new HashSet<Node>();
                openSet.Add(startNode);

                while (openSet.Count > 0)
                {
                    Node node = openSet.RemoveFirst();

                    closedSet.Add(node);

                    if (node == targetNode)
                    {

                        pathSuccess = true;
                        break;
                    }

                    foreach (Node neighbour in node.Neighbors)
                    {
                        if (!neighbour.walkable || closedSet.Contains(neighbour))
                        {
                            continue;
                        }

                        float newCostToNeighbour = node.gCost + GetDistance(node, neighbour);
                        if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                        {
                            neighbour.gCost = newCostToNeighbour;
                            neighbour.hCost = GetDistance(neighbour, targetNode);
                            neighbour.Parent = node;

                            if (!openSet.Contains(neighbour))
                            {
                                openSet.Add(neighbour);

                            }
                            else
                            {
                                openSet.UpdateItem(neighbour);
                            }
                        }
                    }
                }
            }

            yield return null;

            if (pathSuccess)
            {
                waypoints = RetracePath(startNode,targetNode);
            }

            PathRequestManager.FinishedProcessingPath(waypoints,pathSuccess);

        }

        Vector3[] RetracePath(Node startNode, Node endNode) {
            List<Node> path = new List<Node>();
            Node currentNode = endNode;

            while (currentNode != startNode)
            {
                path.Add(currentNode);
                currentNode = currentNode.Parent;
            }
            Vector3[] waypoints = SimplifyPath(path);
            Array.Reverse(waypoints);
            return waypoints;
            //grid.path = path;
        }

        Vector3[] SimplifyPath(List<Node> path) {

            // TODO: DO it for 3D space

            List<Vector3> waypoints = new List<Vector3>();

            for (int i = 1; i < path.Count; i++)
            {
               
               waypoints.Add(path[i].worldPosition);
                
            }
            return waypoints.ToArray();
            
        }

        float GetDistance(Node nodeA, Node nodeB) {
            Vector3 VectorOne = nodeA.worldPosition - TerraformingManager.Planet.position;
            Vector3 VectorTwo = nodeB.worldPosition - TerraformingManager.Planet.position;

            float angle = Vector3.Angle( VectorOne, VectorTwo);

            return angle * pathFindingRadius;
        }
    }
}

