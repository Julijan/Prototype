﻿using UnityEngine;
using System.Collections;
using Poplers;

public class Unit : MonoBehaviour {


	public Transform target;
	public float speed = 1f;
	Vector3[] path;
	int targetIndex;
    public PathRequestManager PathRequestManager;
    Rigidbody rigidbody;
    Vector3 currentWaypoint;

    public float nodeDiameter;

    public float TimeToUpdate = 2f;
    private float LatestTimeUpdated;

    void Start() {
        LatestTimeUpdated = Time.time;
        rigidbody = GetComponent<Rigidbody>();
    }

    void Update() {

        if(Time.time - LatestTimeUpdated > TimeToUpdate)
        {
            LatestTimeUpdated = Time.time;
            PathRequestManager.RequestPath(transform.position, target.position, OnPathFound);
        }
    }

	public void OnPathFound(Vector3[] newPath, bool pathSuccessful) {
		if (pathSuccessful) {
			path = newPath;
			targetIndex = 0;
			StopCoroutine("FollowPath");
			StartCoroutine("FollowPath");
		}
	}

	IEnumerator FollowPath() {
        currentWaypoint = path[0];
		while (true) {
			if (transform.position == currentWaypoint) {
				targetIndex ++;
				if (targetIndex >= path.Length) {
					yield break;
				}
				currentWaypoint = path[targetIndex];
			}

            transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);


            yield return null;

		}
	}

    public void OnDrawGizmos() {
		if (path != null) {
			for (int i = targetIndex; i < path.Length; i ++) {
				Gizmos.color = Color.black;
				Gizmos.DrawCube(path[i], Vector3.one * (nodeDiameter - .1f));

				if (i == targetIndex) {
					Gizmos.DrawLine(transform.position, path[i]);
				}
				else {
					Gizmos.DrawLine(path[i-1],path[i]);
				}
			}
		}
	}
}
