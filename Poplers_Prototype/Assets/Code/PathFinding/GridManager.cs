﻿using System.Collections.Generic;
using UnityEngine;

namespace Poplers
{
    public class GridManager : MonoBehaviour
    {
        // testing A*
        public Transform player;
        public bool DisplayGizmos = false;
        Node playerNode;
        float lastTimeUpdated = 0f;

        public TerraformingManager TerraformingManager;
        public MeshFilter MeshFilter;
        public int VertexCount = 0;
        public LayerMask unwalkableMask;
        public Vector2 gridWorldSize;
        public float nodeRadius;
        Node[] grid;

        float nodeDiameter;

        void Awake() {
            VertexCount = MeshFilter.mesh.vertexCount;
            lastTimeUpdated = Time.time;
            nodeDiameter = nodeRadius * 2;
            CreateGrid();
        }

        public void CreateGrid() {
            grid = new Node[MeshFilter.mesh.vertexCount];

            //add world pos and walkable
            for (int x = 0; x < MeshFilter.mesh.vertexCount; x++)
            {
                Vector3 worldPoint = MeshFilter.mesh.vertices[x];
                bool walkable = IsWalkableNode(x);
                
                grid[x] = new Node(walkable, worldPoint * 1.05f);
            }

            // add neighbors
            for (int x = 0; x < MeshFilter.mesh.vertexCount; x++)
            {
                Vector3 worldPoint = MeshFilter.mesh.vertices[x];

                List<int> indexOfNeighbor = MeshUtils.findAdjacentNeighborIndexes(MeshFilter.mesh.vertices, MeshFilter.mesh.triangles, worldPoint);
                List<Node> neighbors = new List<Node>();

                for (int i = 0; i < indexOfNeighbor.Count; i++)
                {
                    neighbors.Add(grid[indexOfNeighbor[i]]);
                }

                grid[x].Neighbors = neighbors;
            }
               
        }

        public void UpdateGrid(List<int> indexOfVerticesEffected) {
            for (int i = 0; i < indexOfVerticesEffected.Count; i++)
            {
                int vertecisesEffected = indexOfVerticesEffected[i];
                Vector3 worldPoint = MeshFilter.mesh.vertices[vertecisesEffected];
                bool walkable = IsWalkableNode(vertecisesEffected);
                List<Node> cashedNeighbors = grid[vertecisesEffected].Neighbors;

                grid[vertecisesEffected].walkable = walkable;
                grid[vertecisesEffected].worldPosition = worldPoint;
                grid[vertecisesEffected].Neighbors = cashedNeighbors;
            }
        }

        public bool IsWalkableNode(int index) {
            Vector3 vectorOfThisPlanet = MeshFilter.mesh.vertices[index] - TerraformingManager.Planet.position;
            float angleOfSlope = Vector3.Angle(MeshFilter.mesh.normals[index], vectorOfThisPlanet);
            if (angleOfSlope < 20f)
            {
                return true;
            }
            else return false;
        }

        public Node NodeFromWorldPoint(Vector3 worldPosition) {
            return grid[GetNearestPointIndex(worldPosition)];
        }

        public int GetNearestPointIndex(Vector3 pos) {

            float minDistFromPoint = float.MaxValue;
            Vector3 point = Vector3.zero;
            int index = 0;
            List<Vector3> vertices = TerraformingManager.vertices;

            for (int i = 0; i < vertices.Count; i++)
            {
                float distance = Vector3.Distance(pos, vertices[i]);
                if (distance <= minDistFromPoint)
                {
                    minDistFromPoint = distance;
                    point = vertices[i];
                    index = i;
                }
            }
            return index;
        }

        public List<Node> path;

        void OnDrawGizmos() {

            if (DisplayGizmos)
            {
                if (grid != null)
                {

                    foreach (Node n in grid)
                    {
                        Gizmos.color = (n.walkable) ? Color.white : Color.red;
                        if (playerNode == n)
                        {
                            Gizmos.color = Color.cyan;
                        }

                        if (path != null)
                        {
                            if (path.Contains(n) && n.walkable)
                            {
                                Gizmos.color = Color.black;
                            }
                            else if (path.Contains(n) && !n.walkable)
                            {
                                Gizmos.color = Color.green;
                            }
                        }
                        Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - .1f));
                    }

                    // test neigbors

                    //Gizmos.color = Color.blue;
                    //for (int i = 0; i < grid[1].Neighbors.Count; i++)
                    //{
                    //    Gizmos.DrawCube(grid[1].Neighbors[i].worldPosition, Vector3.one * (nodeDiameter - .1f));  
                    //}
                    //Gizmos.color = Color.yellow;
                    //Gizmos.DrawCube(grid[1].worldPosition, Vector3.one * (nodeDiameter - .1f));
                }
            }
            
        }

        private void Update() {
            if(Time.time - lastTimeUpdated > 0.5f)
            {
                lastTimeUpdated = Time.time;
                playerNode = NodeFromWorldPoint(player.position);
            }
        }

    }

    public class Node : IHeapItem<Node>
    {
        public bool walkable;
        public Vector3 worldPosition;
        public List<Node> Neighbors;

        public float gCost;
        public float hCost;

        public Node Parent;
        int heapIndex;
        
        public Node(bool _walkable, Vector3 _worldPos) {
            walkable = _walkable;
            worldPosition = _worldPos;
        }

        public float fCost
        {
            get
            {
                return gCost + hCost;
            }
        }

        public int HeapIndex
        {
            get
            {
                return heapIndex;
            }
            set
            {
                heapIndex = value;
            }
        }

        public int CompareTo(Node nodeToCompare) {
            int compare = fCost.CompareTo(nodeToCompare.fCost);
            if (compare == 0)
            {
                compare = hCost.CompareTo(nodeToCompare.hCost);
            }
            return -compare;
        }
    }
}
