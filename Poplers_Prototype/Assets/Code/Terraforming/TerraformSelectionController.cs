﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Poplers {

    public enum TypeOfTerraform { Up, Down, Flatten };

    public class TerraformSelectionController : MonoBehaviour
    {

        [SerializeField] private TerraformingManager TerraformManager;
        [SerializeField] private GridManager GridManager;
        [SerializeField] private GameObject TerraformSelection;

        public bool TerraformingEnabled;
        public bool Smooth;
        public TypeOfTerraform TypeToTerraform = TypeOfTerraform.Up;

        Vector3 NearestPoint;

        void Update() {

            if (Smooth)
            {
                Smooth = false;
                TerraformManager.Smooth();
            }

            if (TerraformingEnabled)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    TerraformSelection.transform.position = hit.point;
                    TerraformSelection.transform.LookAt(hit.collider.gameObject.transform.position);
                    if (Input.GetMouseButtonDown(0))
                    {
                        NearestPoint = TerraformManager.GetNearestPoint(hit.point);
                    }
                    if (Input.GetMouseButton(0))
                    {
                        List<int> changedVertices = null;
                        if (TypeToTerraform == TypeOfTerraform.Up || TypeToTerraform == TypeOfTerraform.Down) {
                            changedVertices = TerraformManager.TerraformNormal(hit.point, TypeToTerraform);
                        } else if (TypeToTerraform == TypeOfTerraform.Flatten) {
                            changedVertices = TerraformManager.TerraformFlatten(hit.point, NearestPoint);
                        }
                        GridManager.UpdateGrid(changedVertices);

                    }
                }
            }

        }
    }
}

