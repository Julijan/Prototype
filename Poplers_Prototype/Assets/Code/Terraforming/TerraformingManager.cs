﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Poplers
{
    public class TerraformingManager : MonoBehaviour
    {

        public MeshFilter MeshFilter;
        public MeshCollider MeshCollider;
        public List<Vector3> vertices;
        public Transform Planet;
        private Vector3 Center;
        public float Radius;
        public float Speed;

        private float LerpFloat;

        public float MaxHeight;
        public float MinHeight;

        public float TresholdForFlattening;

        public int PowerOfSmoothing = 1;

        private void Awake() {
            MeshFilter.mesh.MarkDynamic();
            vertices = MeshFilter.mesh.vertices.ToList();
            Center = Planet.position;
        }

        public List<int> TerraformNormal(Vector3 pos, TypeOfTerraform typeOfTerraform) {
            List<int> changedVertices = new List<int>();
            for (int i = 0; i < vertices.Count; i++)
            {
                float distance = Vector3.Distance(pos, vertices[i]);
                if (distance < Radius)
                {

                    float step = Speed * Time.deltaTime;

                    if (typeOfTerraform == TypeOfTerraform.Up) vertices[i] = Vector3.MoveTowards(vertices[i], vertices[i] * 1.1f, step);
                    else if (typeOfTerraform == TypeOfTerraform.Down) vertices[i] = Vector3.MoveTowards(vertices[i], vertices[i] * -1.1f, step);

                    changedVertices.Add(i);
                }
            }
            MeshFilter.mesh.SetVertices(vertices);
            MeshFilter.mesh.RecalculateNormals();

            MeshCollider.sharedMesh = null;
            MeshCollider.sharedMesh = MeshFilter.mesh;

            return changedVertices;
        }

        public Vector3 GetNearestPoint(Vector3 pos) {

            float minDistFromPoint = float.MaxValue;
            Vector3 point = Vector3.zero;

            for (int i = 0; i < vertices.Count; i++)
            {
                float distance = Vector3.Distance(pos, vertices[i]);
                if (distance <= minDistFromPoint)
                {
                    minDistFromPoint = distance;
                    point = vertices[i];
                }
            }
            return point;
        }

        public List<int> TerraformFlatten(Vector3 pos, Vector3 referencePoint) {
            if (referencePoint == Vector3.zero) return null; // we didnt find nearest point

            List<int> changedVertices = new List<int>();

            for (int i = 0; i < vertices.Count; i++)
            {
                float distance = Vector3.Distance(pos, vertices[i]);
                if (distance < Radius)
                {
                    float step = Speed * Time.deltaTime;

                    float distanceToCenterReferencePont = Vector3.Distance(referencePoint, Center);
                    float distanceToCenterPointToFlatter = Vector3.Distance(vertices[i], Center);

                    if (Mathf.Abs(distanceToCenterPointToFlatter - distanceToCenterReferencePont) <= TresholdForFlattening) continue;
                    if (distanceToCenterPointToFlatter > distanceToCenterReferencePont)
                    {
                        vertices[i] = Vector3.MoveTowards(vertices[i], vertices[i] * -1.1f, step);
                    }
                    else if (distanceToCenterPointToFlatter < distanceToCenterReferencePont)
                    {
                        vertices[i] = Vector3.MoveTowards(vertices[i], vertices[i] * 1.1f, step);
                    }

                    changedVertices.Add(i);
                }
            }
            MeshFilter.mesh.SetVertices(vertices);
            MeshFilter.mesh.RecalculateNormals();

            MeshCollider.sharedMesh = null;
            MeshCollider.sharedMesh = MeshFilter.mesh;

            return changedVertices;
        }


        public void Smooth() {
            
            // Get the sourceMesh from the originalSkinnedMesh
            Mesh sourceMesh = MeshFilter.mesh;
            // Clone the sourceMesh 
            Mesh workingMesh = CloneMesh(sourceMesh);
            
            // Apply Laplacian Smoothing Filter to Mesh
            for (int i = 0; i < PowerOfSmoothing; i++)
            {
                vertices = SmoothFilter.hcFilter(sourceMesh.vertices, workingMesh.vertices, workingMesh.triangles, 0.0f, 0.5f).ToList();
                sourceMesh.vertices = vertices.ToArray();
                workingMesh.vertices = vertices.ToArray();
            }

            MeshFilter.mesh.SetVertices(vertices);
            MeshFilter.mesh.RecalculateNormals();

            MeshCollider.sharedMesh = null;
            MeshCollider.sharedMesh = MeshFilter.mesh;

        }

        // Clone a mesh
        private static Mesh CloneMesh(Mesh mesh) {
            Mesh clone = new Mesh();
            clone.vertices = mesh.vertices;
            clone.normals = mesh.normals;
            clone.tangents = mesh.tangents;
            clone.triangles = mesh.triangles;
            clone.uv = mesh.uv;
            clone.uv2 = mesh.uv2;
            clone.bindposes = mesh.bindposes;
            clone.boneWeights = mesh.boneWeights;
            clone.bounds = mesh.bounds;
            clone.colors = mesh.colors;
            clone.name = mesh.name;
            //TODO : Are we missing anything?
            return clone;
        }

        //private bool MaxHightReached(Vector3 point, Vector3 center) {
        //    if (Vector3.Distance(point, center) >= MaxHeight) return true;
        //    else return false;
        //}

        //private bool MinHightReached(Vector3 point, Vector3 center) {
        //    if (Vector3.Distance(point, center) <= MinHeight) return true;
        //    else return false;
        //}
    }

}
