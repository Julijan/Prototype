﻿Shader "Custom/Triplanar_mapping" 
{
	Properties 
	{
		_MainTex ("Diffuse Map ", 2D)  = "white" {}
		_OuterTex ("Diffuse Map ", 2D) = "black" {}
		_TextureScale ("Texture Scale",float) = 1
		_TriplanarBlendSharpness ("Blend Sharpness",float) = 1
		_ChangePoint ("Change at this distance", Float) = 3
		_BlendThreshold ("Blend Distance", Float) = 0.5
		_CentrePoint ("Centre", Vector) = (0, 0, 0, 0)
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Lambert

		sampler2D _MainTex;
		float _TextureScale;
		float _TriplanarBlendSharpness;
		float _ChangePoint;
		float _BlendThreshold;
		sampler2D _OuterTex;
		float4 _CentrePoint;

		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
		}; 

		fixed3 TriplanarMapping(sampler2D tex, float3 vertexPosition, float3 vertexNormal, float texScale, float blendSharpness) {
			float3 xn = tex2D(tex, vertexPosition.zy * texScale);
			float3 yn = tex2D(tex, vertexPosition.xz * texScale);
			float3 zn = tex2D(tex, vertexPosition.xy * texScale);

			half3 blendWeightsWorld = pow(abs(vertexNormal), blendSharpness);
			blendWeightsWorld = blendWeightsWorld / (blendWeightsWorld.x + blendWeightsWorld.y + blendWeightsWorld.z);

			return xn * blendWeightsWorld.x + yn * blendWeightsWorld.y + zn * blendWeightsWorld.z;
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{             
             float curDistance = distance(_CentrePoint.xyz, IN.worldPos);

			 if(curDistance<=_ChangePoint){
				o.Albedo = TriplanarMapping(_MainTex, IN.worldPos,IN.worldNormal,_TextureScale, _TriplanarBlendSharpness);
			 }
			 else{
				o.Albedo = TriplanarMapping(_OuterTex, IN.worldPos,IN.worldNormal,_TextureScale, _TriplanarBlendSharpness);
			 }

			
		}
		ENDCG
	}
}