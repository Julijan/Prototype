﻿Shader "Custom/DistanceLerp" {
     Properties {
         _MainTex ("Base (RGB)", 2D) = "white" {}
         _OuterTex ("Base (RGB)", 2D) = "black" {}
		 _ChangePoint ("Change at this distance", Float) = 3
         _CentrePoint ("Centre", Vector) = (0, 0, 0, 0)
         _BlendThreshold ("Blend Distance", Float) = 0.5
     }
     SubShader {
         Tags { "RenderType"="Opaque" }
         LOD 200
         
         CGPROGRAM
         #pragma surface surf Lambert
 
         sampler2D _MainTex;
         float _ChangePoint;
         float4 _CentrePoint;
         sampler2D _OuterTex;
         float _BlendThreshold;
 
         struct Input {
             float2 uv_MainTex;
             float3 worldPos;
         };
 
         void surf (Input IN, inout SurfaceOutput o) {
             half4 main = tex2D (_MainTex, IN.uv_MainTex);
             half4 outer = tex2D (_OuterTex, IN.uv_MainTex);
             
             //float startBlending = _ChangePoint - _BlendThreshold;
             //float endBlending = _ChangePoint + _BlendThreshold;
             
             //float curDistance = distance(_CentrePoint.xyz, IN.worldPos);

             //float changeFactor = saturate((curDistance - startBlending) / (_BlendThreshold * 2));
             
             //half4 c = lerp(main, outer, changeFactor);
             
             //o.Albedo = c.rgb;
             //o.Alpha = c.a;

			 o.Albedo = TriplanarMapping(main, IN.);
         }

		 fixed3 TriplanarMapping(sampler2D tex, float3 vertexPosition, float3 vertexNormal, float texScale, float blendSharpness) {
			float3 xn = tex2D(tex, vertexPosition.zy * texScale);
			float3 yn = tex2D(tex, vertexPosition.xz * texScale);
			float3 zn = tex2D(tex, vertexPosition.xy * texScale);

			half3 blendWeightsWorld = pow(abs(vertexNormal), blendSharpness);
			blendWeightsWorld = blendWeightsWorld / (blendWeightsWorld.x + blendWeightsWorld.y + blendWeightsWorld.z);

			return xn * blendWeightsWorld.x + yn * blendWeightsWorld.y + zn * blendWeightsWorld.z;
		}
        ENDCG
     } 
     FallBack "Diffuse"
 }